from setuptools import setup
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='stretchy-client',
    version='2.1.0',
    description='Python client for the Stretchy database',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/artur-scholz/stretchy-client',
    author='Artur Scholz',
    author_email='artur.scholz@librecube.net',
    license='MIT',
    python_requires='>=3',
    keywords='database client REST api',
    packages=['stretchy_client'],
    install_requires=[
        'requests',
        ],
)
