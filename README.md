# Stretchy-Client

A simple Python3 library for connecting to a [Stretchy database][stretchy].

## Installation

Install via pip:

```
$ pip install stretchy-client
```

## Tutorial

you can run this tutorial in a Python shell or a Jupyter notebook. Before
starting, make sure that Stretchy is installed and running.

### Connect

The first step is to create a client to the Stretchy database:

```python
from stretchy_client import StretchyClient
client = StretchyClient()
```

By default the client expects to find Stretchy at localhost:7979. If Stretchy
is configured differently then this needs to be passed as arguments to
StretchyClient, for example StretchyClient(host="http://example.com", port=5000).

Stretchy data is organized as domains and models. To access the data of models,
a domain and model object must be created.

```python
domain = client['Galaxy']
model = domain['SolarSystem']
```

### Insert Data

To insert data (in the form of documents) into the model, do like so:

```python
documents = [
  {
    'planet': 'Jupiter',
    'moons': 69
  },
  {
    'planet': 'Mars'
  },
  {
    'planet': 'Earth',
    'moons': 1,
    'inhabited': True
  },
]
model.insert(documents)
```

Now open your browser at http://localhost:7979/Galaxy/SolarSystem to see that
these entries were actually created. Note that each entry (i.e. document) gets
an *_id* field and that documents in a model can have different fields.

### Read Data

To read all documents from a model, issue:

```python
documents = model.find()
```

To apply filtering and other query options (see Stretchy documentation for details),
pass these in the method call. For example, get the names of all planets with
that have at least one moon, sorted by number of moons in descending order:

```python
documents = model.find(
    filter={'moons': {'ge': 1}},
    fields=['planet'],
    sort=['-moons']
)
```

### Delete Data

The deletion of documents works in similar fashion as querying. For example, to
delete all planets with more than 10 moons from the database, issue:

```python
model.delete(filter={'moons': {'gt': 10}})
```

To delete the entire model, issue either of the following:

```python
model.delete()
domain.drop(model.name)
domain.drop('SolarSystem')
```

[stretchy]: https://gitlab.com/artur-scholz/stretchy.git
