from .core import StretchyClient


def delete_domain(domain):
    """Deletes a domain and all models within it.

    :param domain: name of the domain.
    :rtype: bool
    """
    client = StretchyClient()
    return client.drop(domain)


def delete_model(domain, model):
    """Deletes a model of a domain.

    :param domain: Name of the domain.
    :param model: Name of the model.
    :rtype: bool
    """
    client = StretchyClient()
    domain = client[domain]
    return domain.drop(model)


def delete_documents(domain, model, filter=None):
    client = StretchyClient()
    domain = client[domain]
    model = domain[model]
    return model.delete(filter)


def load_documents(domain, model, filter=None, fields=None, sort=None,
                   limit=None, page=None, pagesize=None):
    """Loads documents from a domain model.

    :param domain: Name of the domain.
    :param model: Name of the model.
    :param filter: Filter to apply. Set to ``None`` to match all.
    :param fields: (optional) Document fields to be returned.
    :param sort: (optional) Specify sorting of documents.
    :param limit: (optional) Restrict the number of returned documents.
    :param page: (optional) Define which page to return.
    :param pagesize: (optional) Define the size of a page.
    :rtype: list of dicts
    """
    client = StretchyClient()
    domain = client[domain]
    model = domain[model]
    documents = model.find(filter, fields, sort, limit, page, pagesize)
    return documents


def save_documents(domain, model, documents):
    """Saves documents into a domain model.

    :param domain: Name of the domain.
    :param model: Name of the model.
    :param documents: List of dicts.
    :param replace: (optional) If ``True``, the model will be cleared first.
    :rtype: bool
    """
    client = StretchyClient()
    domain = client[domain]
    model = domain[model]
    return model.insert(documents)


def save_dataframe(domain, model, df, datetime_columns=None):
    """Saves a dataframe into a domain model.

    :param domain: Name of the domain.
    :param model: Name of the model.
    :param df: A Pandas dataframe.
    :param replace: (optional) If ``True``, the model will be cleared first.
    :rtype: bool
    """
    client = StretchyClient()
    domain = client[domain]
    model = domain[model]
    if datetime_columns:
        if not isinstance(datetime_columns, list):
            datetime_columns = [datetime_columns]
        for datetime_column in datetime_columns:
            df[datetime_column] = df[datetime_column].apply(
                lambda x: x.isoformat())
    return model.insert_df(df)


def load_dataframe(domain, model, filter=None, fields=None, sort=None,
                   limit=None, page=None, pagesize=None,
                   datetime_columns=None):
    """Loads a dataframe from a domain model.

    :param domain: Name of the domain.
    :param model: Name of the model.
    :param filter: Filter to apply. Set to ``None`` to match all.
    :param fields: (optional) Document fields to be returned.
    :param sort: (optional) Specify sorting of documents.
    :param limit: (optional) Restrict the number of returned documents.
    :param page: (optional) Define which page to return.
    :param pagesize: (optional) Define the size of a page.
    :rtype: dataframe
    """
    import pandas as pd
    documents = load_documents(
        domain, model, filter, fields, sort, limit, page, pagesize)
    df = pd.DataFrame(documents)
    if not df.empty and datetime_columns:
        if not isinstance(datetime_columns, list):
            datetime_columns = [datetime_columns]
        for datetime_column in datetime_columns:
            df[datetime_column] = pd.to_datetime(df[datetime_column])
    return df
