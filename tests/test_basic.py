import unittest

from stretchy_client import StretchyClient


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_default_client_settings(self):
        client1 = StretchyClient()
        client2 = StretchyClient('http://localhost', 7979)
        assert str(client1) == str(client2)


if __name__ == '__main__':
    unittest.main()
